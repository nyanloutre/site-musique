{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation rec {
  name= "site-musique";

  inherit (pkgs) stdenv;

  src = ./.;

  installPhase = ''
    mkdir -p $out/
    cp -R . $out/
  '';

  meta = {
    description = "Site de présentation de l'association Musique Fraternité de Meyenheim";
    homepage = https://musique-meyenheim.fr;
    maintainers = with stdenv.lib.maintainers; [ nyanloutre ];
    license = stdenv.lib.licenses.cc-by-nc-sa-40;
    platforms = stdenv.lib.platforms.all;
  };
}
