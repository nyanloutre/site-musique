<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Site de la musique de Meyenheim">
    <meta name="author" content="Paul TREHIOU">

    <title>Musique fraternité - Partenaires</title>

    <?php include('include_head.html'); ?>
    
        <style>
        @media (min-width: 768px) {
            .navbar-default .nav > li > a,
            .navbar-default .nav > li > a:focus {
                color: #222222;
            }
            .navbar-default .nav > li > a:hover,
            .navbar-default .nav > li > a:focus:hover {
                color: #F05F40;
            }
        }
        
        #partenaires {
            overflow: hidden;
        }
        
        #partenaires a {
            height: 30rem;
        }

        #partenaires a img {
            max-height: 100%;
            max-width: 100%;
        }

    </style>

</head>

<body id="page-top">

    <?php include('navbar.html'); ?>
    
    <section id="partenaires">
        <h1 class="text-center">Nos partenaires</h1>

        <a href="http://www.meyenheim.fr/" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/Meyenheim_logo.jpg"></a>
        <a href="http://ccchr.fr/" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/logo-couleur-CCCHR-e1423820301297.jpg"></a>
        <a href="https://www.haut-rhin.fr/" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/Logo_Alsace_CD68.png"></a>
        <a href="https://culturegrandest.fr/" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/Agence-culturelle-Grand-Est-rouge.jpg"></a>
        <a href="https://www.vialis.tm.fr/particulier/cable/internet" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/vialis.jpg"></a>
        <a href="https://www.creditmutuel.fr/" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/04_credit_mutuel_2012.jpg"></a>
        <a href="https://fr-fr.facebook.com/BoulangerieGoetz/" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/05_boulangerie_goetz2015_1.jpg"></a>
        <a href="http://bapst.chauffagiste-viessmann.fr/" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/Bapst_2017-002.jpg"></a>
        <a href="https://www.planetpizza-leresto.com/" class="col-sm-6 col-md-4"><img class="img-responsive" src="img/partenaires/Planetpizza_1-001.jpg"></a>
    </section>

    <?php include('footer.html'); ?>

    <?php include('include_foot.html'); ?>

</body>

</html>
